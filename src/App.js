
import './App.css';
import { Routes, Route} from 'react-router-dom'
//IMPORT COMPONENTS
import Navbar from './components/navbar/Navbar'
import Footer from './components/footer/Footer'

// iMPORT THE PAGES COMPONETS THAT MERGE IN THE  ONE FOLDER TSKK
import Home from './components/home/Home';
import Login  from './components/login/Login';
import Register from './components/register/Register';
import Cart from './components/cart/Cart';
import Checkout from './components/checkout/Checkout';
import AddressPage from './components/addressPages/AddressPage';
import Create from './components/create/Create';





function App() {
  return (
    <div>
  <Navbar/>
      <Routes>
        <Route path="/" element ={<Home/>} />
        <Route path="/login" element ={<Login/>} />
        <Route path="/register" element ={<Register/>} />
        <Route path="/cart" element ={<Cart/>} />
        <Route path="/checkout" element ={<Checkout/>} />
        <Route path="/create" element ={<Create/>} />
        <Route path="/addressDetails" element ={<AddressPage/>} />
      
      </Routes>
    <Footer/>
      
    </div>
  );
}

export default App;
